"use strict";
var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', function($routeProvider){
    $routeProvider.when('/', {
        templateUrl: "templates/home.html",
        controller: 'homeController'
    })
    .when('/post/:id', {
        templateUrl: 'templates/post.html',
        controller: 'postController'
    })
    .otherwise('/');
}]);

