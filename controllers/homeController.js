app.controller('homeController', ['$scope', 'postService', function($scope, postService){
    /** control visibility of loading */
    $scope.loading = false;

    /**
     * call getList from postService
     * @returns {Promise<*>}
     */
    var getPosts = function(){
        return postService.getList()
        .then(function(result){
            return result.data;
        });
    };
    
    /**
     * the entry point of controller
     */
    $scope.init = function() {
        $scope.loading = true;
        return getPosts()
            .then(function(result){
                $scope.posts = result;
            }).finally(function(){
                $scope.loading = false;
            });
    };

    $scope.init();
}]);