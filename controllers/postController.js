app.controller('postController', ['$scope', '$routeParams', 'postService', function($scope, $routeParams, postService){
    /** control visibility of loading */
    $scope.loading = false;


    /**
     * 
     * @param {number} id
     * @returns {Promise<*>} 
     */
    var getPostById = function(id){
        return postService.getPost(id)
            .then(function(result){
                return result.data;
            });
    };

    /**
     * the entry point of controller
     */
    $scope.init = function(){
        $scope.loading = true;
        
        return getPostById($routeParams.id)
            .then(function(result){
                $scope.post = result;
            }).finally(function(){
                $scope.loading = false;
            });
    };
    $scope.init();
}]);