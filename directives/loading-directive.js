app.directive('loadingObject', [function(){
    return {
        templateUrl: 'templates/directives/loading-directive.html',
        restrict: 'E',
        replace: true
    };
}]);