app.directive('postPreview', [function(){
    return {
        templateUrl: 'templates/directives/post-preview-directive.html',
        restrict: 'E',
        replace: false,
        scope:{
            postTitle: '=',
            postBody: '=',
            postId: '=',
            listFormat: '='
        }
    };
}]);