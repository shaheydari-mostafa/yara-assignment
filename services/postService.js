app.service('postService', ['$http', function($http){
    /**
     * get post list from api
     * @returns {Promise<*>}
     */
    this.getList = function(){
        return $http({
            method:'GET',
            url: 'https://jsonplaceholder.typicode.com/posts'
        });
    };

    /**
     * get post by id
     * @param {number} id
     * @returns {Promise<*>}
     */
    this.getPost = function(id){
        return $http({
            method: 'GET',
            url: 'https://jsonplaceholder.typicode.com/posts/' + id
        });
    };
}]);