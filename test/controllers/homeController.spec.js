describe("home controller", function () {
    beforeEach(module('app'));


    var $controller, $rootScope, postService;
    beforeEach(function () {
        postService = jasmine.createSpyObj('postService', ['getList']);
        postService.getList.and.returnValue(Promise.resolve({
            data: [
                {
                    id: 1,
                    title: 'foo',
                    body: 'bar'
                }
            ]
        }));
    });
    beforeEach(inject(function (_$controller_, _$rootScope_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
    }));

    describe('post array', function(){
        var $scope, controller;
        beforeEach(function(){
            $scope = $rootScope.$new();
            controller = $controller('homeController', { $scope: $scope, postService: postService });
        });

        it('should called getList', function(){
            expect(postService.getList).toHaveBeenCalled();
        });
        
        it('should set correctlly post array length', function () {
            return $scope.init().then(function () {
                expect($scope.posts.length).toEqual(1);
            });
        });

        it('should set correctlly post array value', function(){
            return $scope.init().then(function(){
                expect($scope.posts[0].id).toEqual(1);
            });
        });

    });
});