describe("post controller", function () {
    beforeEach(module('app'));


    var $controller, $rootScope, postService;
    beforeEach(function () {
        postService = jasmine.createSpyObj('postService', ['getPost']);
        postService.getPost.and.returnValue(Promise.resolve({
            data:{
                    id: 1,
                    title: 'foo',
                    body: 'bar'
                }
        }));
    });
    beforeEach(inject(function (_$controller_, _$rootScope_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
    }));

    describe('post object', function () {
        var $scope, controller, routParams;
        beforeEach(function () {
            routParams = {
                id: 1,
            };
            $scope = $rootScope.$new();
            controller = $controller('postController', { $scope: $scope, $routParams: routParams, postService: postService });
        });
        it('should called getPost', function(){
            expect(postService.getPost).toHaveBeenCalled();
        });
        it('should set post object correctlly', function(){
            return $scope.init().then(function(){
                expect($scope.post).toBeDefined();
            });
        });

        it('should post object have valid value', function(){
            return $scope.init().then(function(){
                expect($scope.post.id).toEqual(1);
            });
        });
    });
});