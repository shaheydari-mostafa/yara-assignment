describe('post preview directive', function() {
    var $compile,
        $rootScope;
  
    beforeEach(module('tmpl'));
    beforeEach(module('app'));
  
    beforeEach(inject(function(_$compile_, _$rootScope_){
      $compile = _$compile_;
      $rootScope = _$rootScope_;
    }));
  
    it('check element content', function() {
      var $scope = $rootScope.$new();
      $scope.postTitle = 'foo';
      $scope.postBody = 'bar';
      var element = $compile('<post-preview post-body="postBody" post-title="postTitle"></post-preview>')($scope);
      $scope.$digest();
      expect(element.html()).toContain("foo");
    });
  });